package structurePet.enumstatus;

public enum Status {

    available("available"),  // AVAILABLE
    pending("pending"),   // PENDING
    sold("sold");     // SOLD

    private String value;

    public String getValue() {
        return value;
    }

    Status(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
