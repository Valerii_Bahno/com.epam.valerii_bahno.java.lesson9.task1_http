package pet.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Category{

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private Integer id;

    public String getName(){
        return name;
    }

    public Integer getId(){
        return id;
    }

    @Override
    public String toString(){
        return
                "Category{" +
                        "name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}