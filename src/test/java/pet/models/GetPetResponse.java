package pet.models;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetPetResponse{

    @JsonProperty("photoUrls")
    private List<String> photoUrls;

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("category")
    private Category category;

    @JsonProperty("tags")
    private List<TagsItem> tags;

    @JsonProperty("status")
    private String status;

    public List<String> getPhotoUrls(){
        return photoUrls;
    }

    public String getName(){
        return name;
    }

    public Integer getId(){
        return id;
    }

    public Category getCategory(){
        return category;
    }

    public List<TagsItem> getTags(){
        return tags;
    }

    public String getStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "GetPetResponse{" +
                        "photoUrls = '" + photoUrls + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",category = '" + category + '\'' +
                        ",tags = '" + tags + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}