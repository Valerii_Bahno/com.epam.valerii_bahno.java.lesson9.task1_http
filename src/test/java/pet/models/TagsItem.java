package pet.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TagsItem{

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private Integer id;

    public String getName(){
        return name;
    }

    public Integer getId(){
        return id;
    }

    @Override
    public String toString(){
        return
                "TagsItem{" +
                        "name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}