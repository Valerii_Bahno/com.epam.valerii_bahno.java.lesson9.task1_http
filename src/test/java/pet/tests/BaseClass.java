package pet.tests;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import pet.models.GetPetResponse;
import pet.models.responseapi.ApiResponse;

import java.io.IOException;

public class BaseClass {

    static CloseableHttpClient client;
    static CloseableHttpResponse response;

    protected static String baseUrl = "https://petstore.swagger.io/v2/";
    protected static String baseUrlFindByStatus = "https://petstore.swagger.io/v2/pet/findByStatus?status=";
    protected static int petId = 999;
    protected static String namePet = "Rex";
    protected static String newNamePet = "Mukhtar";
    protected static String urlImagePet = "https://petsi.net/images/dogbreed/10.jpg";

    public static int getPetId() {
        return petId;
    }

    public static String getNamePet() {
        return namePet;
    }

    public static String getNewNamePet() {
        return newNamePet;
    }

    public static String getUrlImagePet() {
        return urlImagePet;
    }

    public GetPetResponse unmarshall(CloseableHttpResponse response, Class<GetPetResponse> classPet) throws IOException {

        String jsonBody = EntityUtils.toString(response.getEntity());

        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .readValue(jsonBody, classPet);
    }

    public ApiResponse unmarshallUploadImage(CloseableHttpResponse response, Class<ApiResponse> classApiPet) throws IOException {

        String jsonBody = EntityUtils.toString(response.getEntity());

        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .readValue(jsonBody, classApiPet);
    }

    @Before
    public void setup(){
        client  = HttpClientBuilder.create().build();
    }

    @After
    public void closeResources() throws IOException {
        client.close();
        response.close();
    }
}
