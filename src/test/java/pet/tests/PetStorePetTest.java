package pet.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pet.models.GetPetResponse;
import pet.models.responseapi.ApiResponse;
import structurePet.enumstatus.Status;
import structurePet.models.Pet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PetStorePetTest extends BaseClass{


    @Test
    public void test1CreateTestPet() throws IOException {
        Pet pet1 = new Pet(getPetId(), getNamePet(), Status.available);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPet = mapper.writeValueAsString(pet1);

        HttpPost request = new HttpPost(baseUrl + "pet");
        request.setEntity(new StringEntity(jsonPet, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test2GetPetId() throws IOException {

        HttpGet get = new HttpGet(baseUrl + "pet/" + getPetId());

        response = client.execute(get);

        GetPetResponse pet = unmarshall(response, GetPetResponse.class);
        int actualStatusCode = response.getStatusLine().getStatusCode();

        assertEquals(pet.getId().intValue(), getPetId());
        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test3GetPetIdNotFound() throws IOException {

        HttpGet get = new HttpGet(baseUrl + "pet/" + 0);

        response = client.execute(get);

        int actualStatusCode = response.getStatusLine().getStatusCode();

        assertEquals(actualStatusCode, HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void test4GetPetName() throws IOException {

        HttpGet get = new HttpGet(baseUrl + "pet/" + getPetId());

        response = client.execute(get);

        GetPetResponse pet = unmarshall(response, GetPetResponse.class);

        assertEquals(pet.getName(), "Rex");
    }

    @Test
    public void test5GetFindByStatus() throws IOException {

        HttpGet get = new HttpGet(baseUrlFindByStatus + Status.available);

        response = client.execute(get);

        int actualStatusCode = response.getStatusLine().getStatusCode();

        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test6PostPetByName() throws IOException {

        Pet pet2 = new Pet(getPetId(), getNewNamePet(), Status.available);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPet = mapper.writeValueAsString(pet2);

        HttpPost request = new HttpPost(baseUrl + "pet");
        request.setEntity(new StringEntity(jsonPet, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test66PostPetByStatus() throws IOException {

        Pet pet2 = new Pet(getPetId(), getNewNamePet(), Status.sold);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPet = mapper.writeValueAsString(pet2);

        HttpPost request = new HttpPost(baseUrl + "pet");
        request.setEntity(new StringEntity(jsonPet, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test7PutExistPet() throws IOException {

        Pet pet7 = new Pet(getPetId(), "new name", Status.pending);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPet = mapper.writeValueAsString(pet7);

        HttpPost request = new HttpPost(baseUrl + "pet");
        request.setEntity(new StringEntity(jsonPet, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        assertEquals(actualStatusCode, HttpStatus.SC_OK);
    }

    @Test
    public void test8PostPetUploadImage() throws IOException {

        List<String> photoUrls = new ArrayList<>();
        photoUrls.add(urlImagePet);

        Pet pet3 = new Pet(getPetId(), photoUrls);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPet = mapper.writeValueAsString(pet3);

        HttpPost request = new HttpPost(baseUrl + getPetId() + "/uploadImage");
        request.setEntity(new StringEntity(jsonPet, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        ApiResponse pet = unmarshallUploadImage(response, ApiResponse.class);

        assertEquals(pet.getCode(), HttpStatus.SC_OK);
    }

    @Test
    public void test9DeletePetIsSuccessful() throws IOException {

        HttpDelete request = new HttpDelete(baseUrl + getPetId());

        response = client.execute(request);

        ApiResponse pet = unmarshallUploadImage(response, ApiResponse.class);

        assertEquals(pet.getCode(), HttpStatus.SC_OK);
    }

}
